import React, {useState, useEffect, useRef, useCallback} from 'react';
import axios from 'axios';

import {MAP_META} from '../constants';
import {
  preprocessTimeseries,
  parseStateTimeseries,
} from '../utils/common-functions';

import Footer from './footer';
import MapExplorer from './mapexplorer';


function Home(props) {
  const [states, setStates] = useState([]);
  const [stateDistrictWiseData, setStateDistrictWiseData] = useState({});
  const [stateTestData, setStateTestData] = useState({});
  const [fetched, setFetched] = useState(false);
  const [graphOption, setGraphOption] = useState(1);
  const [lastUpdated, setLastUpdated] = useState('');
  const [timeseries, setTimeseries] = useState({});
  const [activeStateCode, setActiveStateCode] = useState('TT'); // TT -> India
  const [timeseriesMode, setTimeseriesMode] = useState(true);
  const [timeseriesLogMode, setTimeseriesLogMode] = useState(false);
  const [regionHighlighted, setRegionHighlighted] = useState(undefined);
  const [showUpdates, setShowUpdates] = useState(false);
  const [seenUpdates, setSeenUpdates] = useState(false);
  const [newUpdate, setNewUpdate] = useState(true);

  useEffect(() => {
    // this if block is for checking if user opened a page for first time.
    if (localStorage.getItem('anyNewUpdate') === null) {
      localStorage.setItem('anyNewUpdate', true);
    } else {
      setSeenUpdates(true);
      setNewUpdate(localStorage.getItem('anyNewUpdate') === 'false');
    }
    if (fetched === false) {
      getStates();
      axios
        .get('https://api.covid19india.org/updatelog/log.json')
        .then((response) => {
          const currentTimestamp = response.data
            .slice()
            .reverse()[0]
            .timestamp.toString();
          // Sets and Updates the data in the local storage.
          if (localStorage.getItem('currentItem') !== null) {
            if (localStorage.getItem('currentItem') !== currentTimestamp) {
              localStorage.setItem('currentItem', currentTimestamp);
              localStorage.setItem('anyNewUpdate', true);
            }
          } else {
            localStorage.setItem('currentItem', currentTimestamp);
          }
        })
        .catch((err) => {
          console.log(err);
        });
    }
  }, [fetched]);

  const getStates = async () => {
    try {
      const [
        {data},
        stateDistrictWiseResponse,
        {data: statesDailyResponse},
        {data: stateTestData},
      ] = await Promise.all([
        axios.get('https://api.covid19india.org/data.json'),
        axios.get('https://api.covid19india.org/state_district_wise.json'),
        axios.get('https://api.covid19india.org/states_daily.json'),
        axios.get('https://api.covid19india.org/state_test_data.json'),
      ]);
      setStates(data.statewise);
      const ts = parseStateTimeseries(statesDailyResponse);
      ts['TT'] = preprocessTimeseries(data.cases_time_series); // TT -> India
      setTimeseries(ts);
      setLastUpdated(data.statewise[0].lastupdatedtime);
      const testData = stateTestData.states_tested_data.reverse();
      const totalTest = data.tested[data.tested.length - 1];
      testData.push({
        updatedon: totalTest.updatetimestamp.split(' ')[0],
        totaltested: totalTest.totalindividualstested,
        source: totalTest.source,
        state: 'Total', // India
      });
      setStateTestData(testData);
      setStateDistrictWiseData(stateDistrictWiseResponse.data);
      setFetched(true);
    } catch (err) {
      console.log(err);
    }
  };

  const onHighlightState = (state, index) => {
    if (!state && !index) return setRegionHighlighted(null);
    setRegionHighlighted({state, index});
  };
  const onHighlightDistrict = (district, state, index) => {
    if (!state && !index && !district) return setRegionHighlighted(null);
    setRegionHighlighted({district, state, index});
  };

  const onMapHighlightChange = useCallback(({statecode}) => {
    setActiveStateCode(statecode);
  }, []);

  const refs = [useRef(), useRef(), useRef()];

  return (
    <React.Fragment>
      <div className="Home">
        <div className="home-right">
          {fetched && (
            <React.Fragment>
              <MapExplorer
                forwardRef={refs[1]}
                mapMeta={MAP_META.India}
                states={states}
                stateDistrictWiseData={stateDistrictWiseData}
                stateTestData={stateTestData}
                regionHighlighted={regionHighlighted}
                onMapHighlightChange={onMapHighlightChange}
                isCountryLoaded={true}
              />
            </React.Fragment>
          )}
        </div>
      </div>
      <Footer/>
    </React.Fragment>
  );
}

export default Home;
