import React from 'react';


function Footer(props) {
  return (
    <footer className="fadeInUp" style={{animationDelay: '2s'}}>
      <h5>Developed By: Aniket Khara</h5>
    </footer>
  );
}

export default Footer;
